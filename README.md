# Het maken van een Blogapplicatie

Om de basis van Java Enterprise onder de knie te krijgen is het best om er even 
een realistisch voorbeeld bij te nemen. Laten we eens een webapplicatie bouwen waarop mensen blogs kunnen bijhouden.

### Het domein model

Blogs bevatten verschillende blogposts en behoren altijd tot een bepaalde gebruiker van de applicatie, deze blogposts bevatten een datum/tijdstip van creatie, 
een titel en een inhoud. 

Het is mogelijk dat andere gebruikers commentaren plaatsen op deze blogposts.
Zo'n commentaar of comment is gemaakt door een bepaalde gebruiker en heeft ook een datum/tijdstip.

Gebruikers kunnen twee verschillende rollen aannemen, zo is er buiten gewone gebruikers ook een beheerder van de 
webapplicatie. Gebruikers hebben buiten een rol ook een gebruikersnaam, wachtwoord en een emailadres.