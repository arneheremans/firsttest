package com.heremans.myfirstproject;

public class HelloWorld {

    // door twee slashes te gebruiken schrijf je een lijn in comment, comments worden veel gebruikt om een stuk code te verduidelijken

    // todo door ergens na je slashes het woord 'todo' te plaatsen wordt deze comment nu behandeld als een todo

    // Linksonder in Intelij is er ergens een tab TODO, hierin verschijnt elke todo die ergens in een code bestand zou staan
    // Op die manier kunnen we kleine taken meegeven aan medeontwikkelaars.

    public static void main(String[] args) {

        //this prints 'Hello World!' to the console
        System.out.println("Hello World!");
    }
}
